$("#registro").submit(function(e) {

    e.preventDefault();

    $("#registro").validate({

        ignore: [],

        rules: {

            name: {
                required: true,
                minlength: 3,
                maxlength: 16
            },
            email: {
                required: true,
                email: true,
                maxlength: 255
            }
        },

        messages: {

            name: {
                required: "El nombre es obligatorio",
                minlength: "El mínimo de caracteres requeridos son 3",
                maxlength: "El máximo de caracteres permitidos son 16"
            },
            email: {
                required: "El correo electrónico es obligatorio",
                email: "Debe introducir un email válido",
                maxlength: "El máximo de caracteres permitidos es 255"
            }
        }
    });

    if($("#registro").valid()){

        var formData = new FormData($("#registro")[0]);

        for(var pair of formData.entries()){
            console.log(pair[0] + ', ' + pair[1]);
        };

        $("#ilustration").attr("src","img/smile.png");
        
    }else{

        $("#ilustration").attr("src", "img/sad.png");

    }

});